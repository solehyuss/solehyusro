package com.example.bioproject;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;


public class Quiz extends AppCompatActivity {
    ImageView imageView;
    ImageView imageView2;
    ImageView imageView3;
    EditText ed1;
    TextView tv1,tv2,tv3;
    RadioButton a,b,c,d;
    Button bt;
    RadioGroup rg;
    int q,s;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        imageView = (ImageView) findViewById(R.id.mood);
        imageView2 = (ImageView) findViewById(R.id.badmood);
        imageView3 = (ImageView) findViewById(R.id.moodgajawab);
        ed1 = (EditText) findViewById(R.id.name);
        tv1 = (TextView) findViewById(R.id.ques);
        tv2 = (TextView) findViewById(R.id.response);
        tv3 = (TextView) findViewById(R.id.score);
        rg = (RadioGroup) findViewById(R.id.optionGroup);
        a = (RadioButton) findViewById(R.id.option1);
        b = (RadioButton) findViewById(R.id.option2);
        c = (RadioButton) findViewById(R.id.option3);
        d = (RadioButton) findViewById(R.id.option4);
        bt = (Button) findViewById(R.id.next);
        q = 0;
        s = 0;
    }

    public void MoodBenar()
    {
        imageView.setVisibility(View.VISIBLE);
        imageView.postDelayed(new Runnable() {
            @Override
            public void run() {
                imageView.setVisibility(View.INVISIBLE);
            }
        }, 3000);
    }

    public void MoodSalah()
    {
        imageView2.setVisibility(View.VISIBLE);
        imageView2.postDelayed(new Runnable() {
            @Override
            public void run() {
                imageView2.setVisibility(View.INVISIBLE);
            }
        }, 3000);
    }
    public void MoodGaJawab()
    {
        imageView3.setVisibility(View.VISIBLE);
        imageView3.postDelayed(new Runnable() {
            @Override
            public void run() {
                imageView3.setVisibility(View.INVISIBLE);
            }
        }, 3000);
    }


    public void quiz(View v){
        switch (q){
            case 0:
            {
                ed1.setVisibility(View.INVISIBLE);
                rg.setVisibility(View.VISIBLE);
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                tv2.setText("");
                tv3.setText("");
                a.setEnabled(true);
                b.setEnabled(true);
                c.setEnabled(true);
                d.setEnabled(true);
                ed1.setEnabled(false);
                bt.setText("Next");
                s=0;

                tv1.setText("1. 12 + 10 = ?");
                a.setText("A. 20");
                b.setText("B. 21");
                c.setText("C. 23");
                d.setText("D. 22");
                q=1;
                break;
            }
            case 1:
            {
                ed1.setVisibility(View.INVISIBLE);
                ed1.setEnabled(false);
                tv1.setText("2. 28 - 7 = ?");
                a.setText("A. 19");
                b.setText("B. 20");
                c.setText("C. 21");
                d.setText("D. 22");
                if (d.isChecked()){
                    tv2.setText("Jawaban Benar");
                    s=s+50;
                    MoodBenar();
                }
                else if (!d.isChecked()&&!a.isChecked()&&!b.isChecked()&&!c.isChecked())
                {
                    tv2.setText("Maaf Anda Tidak Menjawab");
                    s=s;
                    MoodGaJawab();
                }
                else if (!d.isChecked())
                {
                    tv2.setText("Maaf Jawaban Anda Salah");
                    s=s-10;
                    MoodSalah();
                }
                q=2;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 2:
            {
                ed1.setVisibility(View.INVISIBLE);
                tv1.setText("3. 6 X 9 = ?");
                a.setText("A. 54");
                b.setText("B. 45");
                c.setText("C. 55");
                d.setText("D. 63");
                if (c.isChecked()){
                    s=s+50;
                    tv2.setText("Jawaban Benar");
                    MoodBenar();
                }
                else if (!d.isChecked()&&!a.isChecked()&&!b.isChecked()&&!c.isChecked())
                {
                    tv2.setText("Maaf Anda Tidak Menjawab");
                    s=s;
                    MoodGaJawab();
                }
                else if (!c.isChecked()){
                    tv2.setText("Maaf Jawaban Anda Salah");
                    s=s-10;
                    MoodSalah();
                }
                q=3;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 3:
            {
                ed1.setVisibility(View.INVISIBLE);
                tv1.setText("4. 72 : 8 = ?");
                a.setText("A. 8");
                b.setText("B. 7");
                c.setText("C. 9");
                d.setText("D. 6");
                if (a.isChecked()){
                    s=s+50;
                    tv2.setText("Jawaban Benar");
                    MoodBenar();
                }
                else if (!d.isChecked()&&!a.isChecked()&&!b.isChecked()&&!c.isChecked())
                {
                    tv2.setText("Maaf Anda Tidak Menjawab");
                    s=s;
                    MoodGaJawab();
                }
                else if (!a.isChecked()){
                    tv2.setText("Maaf Jawaban Anda Salah");
                    s=s-10;
                    MoodSalah();
                }
                q=4;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 4:
            {
                ed1.setVisibility(View.INVISIBLE);
                ed1.setEnabled(true);
                tv1.setText("5. 25 : 5 = ?");
                a.setText("A. 3");
                b.setText("B. 4");
                c.setText("C. 5");
                d.setText("D. 6");
                if (c.isChecked()){
                    tv2.setText("Jawaban Benar");
                    s=s+50;
                    MoodBenar();
                }
                else if (!d.isChecked()&&!a.isChecked()&&!b.isChecked()&&!c.isChecked())
                {
                    tv2.setText("Maaf Anda Tidak Menjawab");
                    s=s;
                    MoodGaJawab();
                }
                else if (!c.isChecked())
                {
                    tv2.setText("Maaf Jawaban Anda Salah");
                    s=s-10;
                    MoodSalah();
                }
                q=5;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 5:
            {
                ed1.setVisibility(View.INVISIBLE);
                tv1.setText("6. 4 X 6 = ?");
                a.setText("A. 24");
                b.setText("B. 23");
                c.setText("C. 22");
                d.setText("D. 21");
                if (c.isChecked()){
                    s=s+50;
                    tv2.setText("Jawaban Benar");
                    MoodBenar();
                }
                else if (!d.isChecked()&&!a.isChecked()&&!b.isChecked()&&!c.isChecked())
                {
                    tv2.setText("Maaf Anda Tidak Menjawab");
                    s=s;
                    MoodGaJawab();
                }
                else if (!c.isChecked()){
                    tv2.setText("Maaf Jawaban Anda Salah");
                    s=s-10;
                    MoodSalah();
                }
                q=6;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 6:
            {
                ed1.setVisibility(View.INVISIBLE);
                tv1.setText("7. 124 - 24 = ?");
                a.setText("A. 98");
                b.setText("B. 99");
                c.setText("C. 100");
                d.setText("D. 101");
                if (a.isChecked()){
                    s=s+50;
                    tv2.setText("Jawaban Benar");
                    MoodBenar();
                }
                else if (!d.isChecked()&&!a.isChecked()&&!b.isChecked()&&!c.isChecked())
                {
                    tv2.setText("Maaf Anda Tidak Menjawab");
                    s=s;
                    MoodGaJawab();
                }
                else if (!a.isChecked()){
                    tv2.setText("Maaf Jawaban Anda Salah");
                    s=s-10;
                    MoodSalah();
                }
                q=7;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 7:
            {
                ed1.setVisibility(View.INVISIBLE);
                tv1.setText("8. 65 + 20 = ?");
                a.setText("A. 85");
                b.setText("B. 75");
                c.setText("C. 65");
                d.setText("D. 95");
                if (c.isChecked()){
                    s=s+50;
                    tv2.setText("Jawaban Benar");
                    MoodBenar();
                }
                else if (!d.isChecked()&&!a.isChecked()&&!b.isChecked()&&!c.isChecked())
                {
                    tv2.setText("Maaf Anda Tidak Menjawab");
                    s=s;
                    MoodGaJawab();
                }
                else if (!c.isChecked()){
                    tv2.setText("Maaf Jawaban Anda Salah");
                    s=s-10;
                    MoodSalah();
                }
                q=8;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 8:
            {
                ed1.setVisibility(View.INVISIBLE);
                tv1.setText("9. 41 - 9 = ?");
                a.setText("A. 34");
                b.setText("B. 33");
                c.setText("C. 32");
                d.setText("D. 31");
                if (a.isChecked()){
                    s=s+50;
                    tv2.setText("Jawaban Benar");
                    MoodBenar();
                }
                else if (!d.isChecked()&&!a.isChecked()&&!b.isChecked()&&!c.isChecked())
                {
                    tv2.setText("Maaf Anda Tidak Menjawab");
                    s=s;
                    MoodGaJawab();
                }
                else if (!a.isChecked()){
                    tv2.setText("Maaf Jawaban Anda Salah");
                    s=s-10;
                    MoodSalah();
                }
                q=9;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 9:
            {
                ed1.setVisibility(View.INVISIBLE);
                tv1.setText("10. 81 : 9 = ?");
                a.setText("A. 7");
                b.setText("B. 8");
                c.setText("C. 10");
                d.setText("D. 9");
                if (c.isChecked()){
                    s=s+50;
                    tv2.setText("Jawaban Benar");
                    MoodBenar();
                }
                else if (!d.isChecked()&&!a.isChecked()&&!b.isChecked()&&!c.isChecked())
                {
                    tv2.setText("Maaf Anda Tidak Menjawab");
                    s=s;
                    MoodGaJawab();
                }
                else if (!a.isChecked()){
                    tv2.setText("Maaf Jawaban Anda Salah");
                    s=s-10;
                    MoodSalah();
                }
                q=10;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 10:
            {
                ed1.setVisibility(View.INVISIBLE);
                a.setEnabled(false);
                b.setEnabled(false);
                c.setEnabled(false);
                d.setEnabled(false);
                bt.setText("Finish");
                if (d.isChecked()){
                    s=s+25;
                    tv2.setText("Jawaban Benar");
                    MoodBenar();
                }
                else if (!d.isChecked()&&!a.isChecked()&&!b.isChecked()&&!c.isChecked())
                {
                    tv2.setText("Maaf Anda Tidak Menjawab");
                    s=s;
                    MoodGaJawab();
                }
                else if (!d.isChecked()){
                    tv2.setText("Maaf Jawaban Anda Salah");
                    s=s-10;
                    MoodSalah();
                }

                tv3.setText(ed1.getText()+" Skor Akhir Anda Adalah "+s);
                bt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Quiz.this, MainActivity.class);
                                startActivity(intent);
                    }
                });
                q=0;
                break;
            }
        }
    }
}